export interface INote {
    id: string
    createdAt?: Date
    updatedAt?: Date
    title: string
    text: string
    userId: string
}