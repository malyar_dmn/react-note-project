import React, { useEffect } from 'react';
import './App.css';
import axios from 'axios';
import { Col, Layout, Row } from 'antd';
import { NotesList } from './components/NotesList';
import { AppRouter } from './components/AppRouter';
import { Navbar } from './components/Navbar';

const { Header, Footer, Sider, Content } = Layout;

function App() {

  // useEffect(() => {
  //   axios.post('http://localhost:5000/notes', {title: 'title from react', text: 'text from react'})
  //     .then(res => console.log(res))
  // }, [])

  return (
    <Layout>
      <Navbar />
      <Layout.Content>
        <AppRouter />
      </Layout.Content>
    </Layout>
  );
}

export default App;
