import { Col, Layout, Row } from 'antd'
import React from 'react'
import { AddNote } from '../components/AddNote'
import { NoteDetails } from '../components/NoteDetails'
import { NotesList } from '../components/NotesList'
import { useGlobalContext } from '../context/context'

export const Notes: React.FC = () => {
    const {state} = useGlobalContext()
    return (
        <Layout className='note-screen-wrapper'>
            <Row style={{height: '100%'}}>
                <Col span={6} style={{height: '100%'}}>
                    <NotesList />
                </Col>
                <Col flex="auto">
                    {state.isAddNote
                        ?
                        <AddNote />
                        :
                        <NoteDetails />
                    }
                </Col>
            </Row>
        </Layout>
    )
}
