import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import GlobalContextProvider from './context/context';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
  <GlobalContextProvider>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </GlobalContextProvider>,
  document.getElementById('root')
);

