import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import {Switch} from 'react-router-dom'
import { useGlobalContext } from '../context/context'
import { privateRoutes, publicRoutes, RouteNames } from '../router'

export const AppRouter = () => {
    const {state} = useGlobalContext()
    return (
        state.token ?
            <Switch>
                {privateRoutes.map(route => (
                    <Route path={route.path} exact={route.exact} component={route.component} key={route.path} />
                ))}
                <Redirect to={RouteNames.NOTES} />
            </Switch>
            :
            <Switch>
                {publicRoutes.map(route => (
                    <Route path={route.path} exact={route.exact} component={route.component} key={route.path} />
                ))}
                <Redirect to={RouteNames.LOGIN} />
            </Switch>
    )
}
