import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import { List } from 'antd'
import React from 'react'
import { useGlobalContext } from '../context/context'
import { INote } from '../models/INote'

interface Props {
    note: INote
}

export const NoteItem: React.FC<Props> = ({note}) => {
    const {selectNote, onEditNoteHandler} = useGlobalContext()

    return (
        <List.Item className='note-item' onClick={() => selectNote(note.id)}>
            <p style={{margin: '0'}}>{note.title}</p>
            <div style={{flexDirection: 'row', justifyContent: 'flex-end', gap: '10px'}}>
            <EditOutlined style={{fontSize: '1.5rem'}} onClick={onEditNoteHandler}/>
            <DeleteOutlined style={{fontSize: '1.5rem'}}/>
            </div>
        </List.Item>
    )
}
