import { Form, Input, Button } from 'antd'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useGlobalContext } from '../context/context'

export const LoginForm: React.FC = () => {
    const {login, state} = useGlobalContext()

    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const submit = () => {
        login(username, email, password)
        console.log('after login', state)
    }

    return (
        <Form onFinish={submit} labelCol={{span: 8}} wrapperCol={{span: 16}} labelAlign='left'>
            <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input onChange={(e) => setUsername(e.target.value)} />
            </Form.Item>

            <Form.Item
                label="Email"
                name="email"
                rules={[{ required: true, message: 'Please input your email!' }]}
            >
                <Input onChange={(e) => setEmail(e.target.value)} />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password onChange={(e) => setPassword(e.target.value)} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                <Button type="primary" htmlType="submit">Submit</Button>
            </Form.Item>
        </Form>
    )
}
