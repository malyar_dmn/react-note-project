import { Layout, Menu, Row } from 'antd'
import React from 'react'
import { useHistory } from 'react-router-dom'
import { useGlobalContext } from '../context/context'
import { RouteNames } from '../router'

export const Navbar: React.FC = () => {
    const {state, logout} = useGlobalContext()
    const router = useHistory()

    const handleLogout = () => {
        router.push(RouteNames.LOGIN)
        logout()
    }

    return (
        <Layout.Header>
            <Row justify='end'>
                {state.token
                    ?
                    <>
                        <div style={{color: '#fff'}}>{state.user?.username}</div>
                        <Menu theme='dark' mode='horizontal' selectable={false}>
                            <Menu.Item onClick={() => handleLogout()} key={1} style={{color: '#fff'}}>Logout</Menu.Item>
                        </Menu>
                    </>
                    :
                    <>
                        <Menu theme='dark' mode='horizontal' selectable={false}>
                            <Menu.Item onClick={() => router.push(RouteNames.LOGIN)} key={2} style={{color: '#fff'}}>Login</Menu.Item>
                        </Menu>
                    </>
                    }

            </Row>
        </Layout.Header>
    )
}
