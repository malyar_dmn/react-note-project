import { Button, Form, Input } from 'antd'
import { useState } from 'react'
import { useGlobalContext } from '../context/context'

export const AddNote = () => {
    const {addNewNote, editExistingNote, onCancel, state} = useGlobalContext()

    const [noteTitle, setNoteTitle] = useState('')
    const [noteText, setNoteText] = useState('')


    const onAddNote = () => {
        if (noteTitle && state.selectedNoteId === 'custom_note_id') {
            addNewNote(noteTitle, noteText)
            return
        }

        editExistingNote(noteTitle, noteText)
    }

    return (
        <Form className='add-note-form' onFinish={onAddNote}>
            <Form.Item rules={[{required: true, message: 'Please input note title'}]}>
                <Input onChange={(e) => setNoteTitle(e.target.value)}/>
            </Form.Item>
            <Form.Item style={{flexGrow: 1}} rules={[{required: true, message: 'Please input note text'}]}>
                <Input.TextArea style={{height: '100%'}} onChange={(e) => setNoteText(e.target.value)}/>
            </Form.Item>
            <Form.Item>
                <Button htmlType='submit'>Add note</Button>
                <Button htmlType='submit' onClick={onCancel}>Cancel</Button>
            </Form.Item>
        </Form>
    )
}
