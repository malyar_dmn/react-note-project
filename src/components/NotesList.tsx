import { Button, Layout, List } from 'antd'
import { useGlobalContext } from '../context/context'
import { NoteItem } from './NoteItem'
import { INote } from '../models/INote'


export const NotesList = () => {
  const {state, addEmptyNote} = useGlobalContext()

    return (
        <List 
          size='large'
          style={{height: '100%'}}
          bordered
          header={<Button style={{textAlign: 'center'}} onClick={() => addEmptyNote()} disabled={state.isAddNote}>+</Button>}
          dataSource={state.notes}
          rowKey={item => item.id}
          renderItem={(item: INote) => <NoteItem note={item}/>}
        />
    )
}
