import { Card } from 'antd'
import React, { useEffect } from 'react'
import { useGlobalContext } from '../context/context'


export const NoteDetails: React.FC = () => {
    const {state, getSelectedNote} = useGlobalContext()
    const noteDetails = getSelectedNote()

    useEffect(() => {
        console.log('state', state)
    }, [state.selectedNoteId])

    if (!noteDetails) return <div>No data</div>

    return (
        <>
            {state.selectedNoteId
                ?
                <Card title={noteDetails?.title} style={{height: '100%'}}>
                    <p>{noteDetails?.text}</p>
                </Card>
                :
                <p>Note hasn't been selected</p>
            }
        </>
    )
}
