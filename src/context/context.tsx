import { createContext, useContext, useEffect, useState } from 'react'
import axios from "axios";
import { IUser } from '../models/IUser';
import { INote } from '../models/INote';

const GlobalContext = createContext<GlobalController>({} as GlobalController)

interface Props {
    children: any;
}

const GlobalContextProvider = (props: Props) => {
    const [data, setData] = useState<GlobalState>({
        user: null,
        notes: [],
        token: '',
        selectedNoteId: '',
        isAddNote: false,
        editNoteId: ''
    })

    useEffect(() => {
        getNotes()
    }, [data.token])


    const login = (username: string, email: string, password: string) => {
        axios.post('http://localhost:5000/auth/login', {
            username,
            email,
            password
        }).then(res => {
            console.log('res context', res)
            setData(prevState => {
                return {
                    ...prevState,
                    token: res.data.token,
                    user: res.data.user
                }
            })
        })
    }

    const logout = () => {
        setData(prevState => {
            return {
                ...prevState,
                token: '',
                user: null
            }
        })
        
    }


    const selectNote = (id: string) => {
        setData(prevState => {
            return {
                ...prevState,
                selectedNoteId: id,
                isAddNote: false
            }
        })
        onCancel()
    }

    const onEditNoteHandler = () => {
        setData(prevState => {
            return {
                ...prevState,
                isAddNote: true
            }
        })
    }

    const getSelectedNote = () => {
        return data.notes.find(item => item.id === data.selectedNoteId)
    }

    const addEmptyNote = () => {
        const emptyNote: INote = {
            title: 'Enter note title',
            text: '',
            id: 'custom_note_id',
            userId: data.user!.id
        }
        setData(prevState => {
            return {
                ...prevState,
                notes: [...prevState.notes, emptyNote],
                selectedNoteId: 'custom_note_id',
                isAddNote: true
            }
        })
    }


    const addNewNote = (title: string, text: string) => {

        console.log('add note', title, text)
        // const config = {
        //     headers: {
        //         'Authorization': `Bearer ${data.token}`
        //     }
        // }
        // axios.post('http://localhost:5000/notes', {
        //     title,
        //     text,
        // }, config).then(res => {
        //     setData(prevState => {
        //         return {
        //             ...prevState,
        //             notes: [...prevState.notes, res.data]
        //         }
        //     })
        // })
        // console.log('array of notes', data.notes)
    }

    const onCancel = () => {
        setData(prevState => {
            return {
                ...prevState,
                notes: [...prevState.notes.filter(note => note.id !== 'custom_note_id')]
            }
        })
    }

    const editExistingNote = (title: string, text: string): void => {
        let note = data.notes.find(item => item.id === data.selectedNoteId)
        if (note) {
            note.title = title
            note.text = text
        }
        setData(prevState => {
            return {
                ...prevState,
                isAddNote: false
            }
        })
    }

    const setUser = (user: IUser) => {
        setData(prevState => {
            return {
                ...prevState,
                user
            }
        })

        console.log('context is user', data.user)
    }

    const setToken = (token: string) => {
        setData(prevState => {
            return {
                ...prevState,
                token
            }
        })
        console.log('token set token', data.token)
    }

    const getNotes = () => {
        axios.get('http://localhost:5000/notes', {
            headers: {
                'Authorization': `Bearer ${data.token}`
            }
        }).then(res => {
            setData(prevState => {
                return {
                    ...prevState,
                    notes: res.data
                }
            })
        })
    }



    const globalState: GlobalController = {
        state: data,
        setUser,
        getNotes,
        setToken,
        login,
        logout,
        addNewNote,
        getSelectedNote,
        selectNote,
        addEmptyNote,
        onEditNoteHandler,
        editExistingNote,
        onCancel
    }

    return (
        <GlobalContext.Provider value={globalState}>
            {props.children}
        </GlobalContext.Provider>
    )
}

export interface GlobalController {
    state: GlobalState
    setUser: (user: IUser) => void
    getNotes: () => void
    login: (username: string, email: string, password: string) => void
    setToken: (token: string) => void
    logout: () => void
    addNewNote: (title: string, text: string) => void
    getSelectedNote: () => INote | undefined
    selectNote: (id: string) => void
    addEmptyNote: () => void
    onEditNoteHandler: () => void
    editExistingNote: (title: string, text: string) => void
    onCancel: () => void
}


export interface GlobalState {
    user: IUser | null
    token: string
    notes: INote[]
    selectedNoteId: string
    isAddNote: boolean
    editNoteId: string
}


export default GlobalContextProvider

export const useGlobalContext = () => useContext<GlobalController>(GlobalContext)